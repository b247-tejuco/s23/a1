let trainer= {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log("Pikachu! I choose you!")
	}
}
console.log(trainer);
console.log('Result of dot notation:')
console.log(trainer.name)
console.log('Result of square bracket notation:')
console.log(trainer['pokemon'])
console.log('Result of talk method')
trainer.talk();

function Pokemon(name, level){
	this.name= name;
	this.level= level;
	this.health= 2 * level;
	this.attack= level;
	this.tackle= function(target){
		console.log(this.name+ ' tackled '+ target.name);
		console.log(target.name+"'s health is now reduced to "+ Number(target.health - this.attack));
		target.health= (target.health - this.attack)

		if(target.health <= 0){

				target.faint()
			} 
		}
	this.faint= function(){
		console.log(this.name+' fainted.');
	}

	}


let pokemon1= new Pokemon('Pikachu', 12)
let pokemon2= new Pokemon('Geodude', 8)
let pokemon3= new Pokemon('Mewtwo', 100)
console.log(pokemon1)
console.log(pokemon2)
console.log(pokemon3)

pokemon2.tackle(pokemon1);
console.log(pokemon1)

pokemon3.tackle(pokemon2);
console.log(pokemon2);